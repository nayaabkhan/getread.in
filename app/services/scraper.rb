require 'nokogiri'
require 'open-uri'

class Scraper
  attr_accessor :params

  def self.fetch(source)
    case source.site.slug
    when 'hindustan-times'
      from_hindustan_times(source)
    when 'asian-age'
      from_asian_age(source)
    when 'livemint'
      from_livemint(source)
    when 'economic-times-blog'
      from_economic_times_blog(source)
    when 'the-indian-express'
      from_the_indian_express(source)
    when 'firstpost'
      from_firstpost(source)
    when 'business-standard'
      from_business_standard(source)
    when 'swarajya'
      from_swarajya(source)
    when 'the-new-indian-express'
      from_the_new_indian_express(source)
    when 'ndtv'
      from_ndtv(source)
    when 'times-of-india'
      from_times_of_india(source)
    when 'the-pioneer'
      from_the_pioneer(source)
    end
  end

  def self.from_hindustan_times(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.searchNews li').each do |story|
      url = story.at_css('h2 a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('h2').text.strip
      synopsis = story.at('.web-summary p').text.strip
      published_at = Utilities::parse_date(story.at_css('span').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_asian_age(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.view-columnists .view-content .views-row').each do |story|
      url = source.site.homepage + story.at_css('.node h2 a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('.node h2 a').text.strip
      synopsis = story.at_css('.node p:nth-child(3)').text.strip

      # no date on the listing page, so we open the article and get the date
      article_page = Nokogiri::HTML(open(url))
      published_at = Utilities::parse_date(article_page.at_css('.byline .submitted').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_livemint(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.listing-box-container .listing-box').each do |story|
      url = source.site.homepage + story.at_css('.split-heading-strong a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('.split-heading-strong a').text.strip
      synopsis = story.at_css('.intro a').text.strip
      published_at = Utilities::parse_date(story.at_css('.date-box').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_economic_times_blog(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.feeds .article').each do |story|
      url = story.at_css('.media-body .media-heading a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('.media-body .media-heading a').text.strip
      synopsis = story.at_css('.media-body .content a').text.strip
      published_at = Utilities::parse_date(story.at_css('.media-meta .date').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_the_indian_express(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.col-stories').each do |story|
      url = story.at_css('h6 a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('h6 a').text.strip
      synopsis = story.at_css('p').text.strip
      published_at = Utilities::parse_date(story.at_css('.date').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_firstpost(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.listlftmn').each do |story|
      url = story.at_css('.sport_tit a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('.sport_tit a').text.strip
      synopsis = story.at_css('.sport_desc > text()').text.strip
      published_at = Utilities::parse_date(story.at_css('.date_ago').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_business_standard(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.story-content .aticle-list ul li').each do |story|
      url = source.site.homepage + story.at_css('h4 a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('h4 a > text()').text.strip
      synopsis = story.at_css('p.black').text.strip
      published_at = Utilities::parse_date(story.at_css('p.date').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_swarajya(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.blog-listing ul li').each do |story|
      url = story.at_css('article h3 a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('article h3 a').text.strip
      synopsis = story.at_css('article > p').text.strip
      published_at = Utilities::parse_date(story.at_css('article .meta-tags a[rel="date"]').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_the_new_indian_express(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.widget.list .widget-title + .content ul li').each do |story|
      url = story.at_css('a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('a').text.strip
      published_at = Utilities::parse_date(story.at_css('.dateLine').text.strip)

      Story.create({:source => source, :title => title, :url => url, :published_at => published_at})
    end
  end

  def self.from_ndtv(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.opinion_blog_list ul li').each do |story|
      url = story.at_css('.opinion_blog_header a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('.opinion_blog_header a').text.strip
      synopsis = story.at_css('.opinion_blog_intro').text.strip
      published_at = Utilities::parse_date(story.at_css('.opinion_blog_dateline').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_times_of_india(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('.media.article').each do |story|
      url = story.at_css('.media-heading a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('.media-heading a').text.strip
      synopsis = story.at_css('.content p').text.strip
      published_at = Utilities::parse_date(story.at_css('.media-meta .date').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end

  def self.from_the_pioneer(source)
    already_scraped_urls = source.stories.pluck(:url)

    document = Nokogiri::HTML(open(source.url))
    document.css('#most_viewed_l #most_v_brief').each do |story|
      url = source.site.homepage + story.at_css('.news_title a')[:href]
      next if (already_scraped_urls.include?(url))

      title = story.at_css('.news_title a').text.strip
      synopsis = story.at_css('> p').text.strip
      published_at = Utilities::parse_date(story.at_css('.date_time').text.strip)

      Story.create({:source => source, :title => title, :url => url, :synopsis => synopsis, :published_at => published_at})
    end
  end
end
