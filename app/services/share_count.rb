require "net/http"
require "uri"
require "json"

class ShareCount
  @@facebook_api_url = "https://graph.facebook.com/?ids=%s"

  def self.for_facebook(url)
    url = URI.encode(url)
    uri = URI.parse(@@facebook_api_url % url)

    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    http.verify_mode = OpenSSL::SSL::VERIFY_NONE

    request = Net::HTTP::Get.new(uri.request_uri)
    response = http.request(request)

    begin
      count = JSON.parse(response.body)[url]['shares'].to_i
      return count
    rescue Exception => e
      return 0
    end
  end
end
