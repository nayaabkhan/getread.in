module ApplicationHelper
  def title(page_title)
    content_for(:title) { page_title }
  end

  def nav_link(text, path, options = {})
    active_modifier = '--active'
    options[:class] = options[:class] + active_modifier if current_page?(path)
    content_tag(:li, link_to(text, path), options)
  end

  def site_link(text, path, options = {})
    active_modifier = '--active'

    recognized = Rails.application.routes.recognize_path(path)
    options[:class] = options[:class] + active_modifier if recognized[:controller] == params[:controller]
    link_to(text, path, options)
  end

  def inline_svg(filename, options = {})
    file = File.read(Rails.root.join('app', 'assets', 'images', filename))
    doc = Nokogiri::HTML::DocumentFragment.parse file
    svg = doc.at_css 'svg'
    if options[:class].present?
      svg['class'] = options[:class]
    end
    doc.to_html.html_safe
  end
end
