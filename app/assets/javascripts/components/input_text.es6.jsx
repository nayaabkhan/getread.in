class InputText extends React.Component {
  constructor(props) {
    super(props);
  }

  handleOnChange(e) {
    this.props.onChange(e.target.value);
  }

  handleKeyDown(e) {
    if (e.keyCode == 13) {
      this.props.onSubmit();
    }
  }

  render () {
    return (
      <input
        className="input"
        type="text"
        name={this.props.name}
        placeholder={this.props.placeholder}
        value={this.props.value}
        onChange={this.handleOnChange.bind(this)}
        onKeyDown={this.handleKeyDown.bind(this)}
      />
    );
  }
}

InputText.propTypes = {
  name: React.PropTypes.string,
  placeholder: React.PropTypes.string,
  value: React.PropTypes.string,
  onChange: React.PropTypes.func,
  onSubmit: React.PropTypes.func
};
