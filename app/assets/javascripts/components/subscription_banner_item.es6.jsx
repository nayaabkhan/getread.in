class SubscriptionBannerItem extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      show_success_banner: false,
      email_address: '',
      message: ''
    };
  }

  handleSubscriptionSubmit() {
    var emailAddress = this.state.email_address;
    var re = /@/i;

    if (emailAddress == '' || !re.test(emailAddress)) {
      alert("The Email you gave isn't a valid one.");
    } else {
      $.ajax({
        type: 'POST',
        url: '/subscribe',
        data: { email_address: emailAddress }
      })
      .done(function (data) {
        if (data.success) {
          this.setState({ show_success_banner: true });
        }
        this.setState({ message: data.message });
      }.bind(this));
    }
  }

  handleEmailAddressChange(newEmailAddress) {
    this.setState({ email_address: newEmailAddress });
  }

  render() {
    const subscriptionBanner =
      <div className="subscription-banner-item">
        <span className="l-subscription-copy">
          <h4 className="subscription-title">
            Get best stories of the week in your mail
          </h4>
          <p className="subscription-para">
            Subscribe to the weekly news digest and get best stories of the
            week delivered to you. Don"t worry, we don"t SPAM.
          </p>
        </span>
        <span className="l-subscription-form-holder">
          <div className="l-subscription-form">
            <span className="l-subscription-form-input-cell">
              <InputText
                name="email_address"
                placeholder="Email address"
                value={this.state.email_address}
                onChange={this.handleEmailAddressChange.bind(this)}
                onSubmit={this.handleSubscriptionSubmit.bind(this)}
              />
            </span>
            <span className="l-subscription-form-button-cell">
              <Button
                onClick={this.handleSubscriptionSubmit.bind(this)}
                label="Subscribe"
              />
            </span>
          </div>
        </span>
      </div>;

    const successBanner =
      <div className="l-subscription-message">
        {this.state.message}
      </div>;

    var toRender = null;
    if (this.state.show_success_banner) {
      toRender = successBanner;
    } else {
      toRender = subscriptionBanner;
    }

    return toRender;
  }
}

