class Button extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <button
        className="button--green"
        onClick={this.props.onClick.bind(this)}
      >
        {this.props.label}
      </button>
    );
  }
}

Button.propTypes = {
  label: React.PropTypes.string,
  onClick: React.PropTypes.func
};
