// via https://sheharyar.me/blog/using-google-analytics-in-rails-4-with-turbolinks
jQuery(function() {
  return $(document).on('page:change', function() {
    if (window.ga != null) {
      ga('set', 'location', location.href.split('#')[0]);
      return ga('send', 'pageview', {
        "title": document.title
      });
    }
  });
});
