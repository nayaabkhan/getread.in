var menuOpen = false;

var initResponsiveSearch = function () {
  if ($(window).width() > 720) return;

  $('.search-label, .search-close').click(function (event) {
    $('.search').toggleClass('is-active');
    $('.search-input').toggle();
    $('.search-label').toggle();
    $('.search-close').toggle();
  });
};

$(document).ready(initResponsiveSearch)
$(document).on('page:load', initResponsiveSearch)
