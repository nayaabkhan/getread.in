var menuOpen = false;

var initResponsiveNav = function () {
  if ($(window).width() > 600) return;

  $('.nav').click(function (event) {
    if (menuOpen) {
      $(this).removeClass('is-open');
      $(this).find('.nav-item').hide();
      menuOpen = !menuOpen;
      return true;
    } else {
      $(this).addClass('is-open');
      $(this).find('.nav-item').show();
      menuOpen = !menuOpen;
      return false;
    }
  });
};

$(document).ready(initResponsiveNav)
$(document).on('page:load', initResponsiveNav)
