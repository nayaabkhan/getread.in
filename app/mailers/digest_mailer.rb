class DigestMailer < ApplicationMailer
  default from: "GetRead Digest <hello@getread.in>"
  layout 'mailer', except: [:welcome]

  include Roadie::Rails::Mailer

  def welcome(email_address)
    roadie_mail(to: email_address, subject: "We've signed you up!")
  end

  def send_weekly_digest(top_stories)
    @top_stories = top_stories
    roadie_mail(to: "weeklydigest@mg.getread.in", subject: "Your weekly GetRead Digest is here.")
  end
end
