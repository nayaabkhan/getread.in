class ApplicationMailer < ActionMailer::Base
  default from: "contact@getread.in"
  layout 'mailer'
end
