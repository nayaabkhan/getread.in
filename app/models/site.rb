class Site < ActiveRecord::Base
  has_many :sources

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }
  validates :slug, presence: true
  validates :slug, uniqueness: { case_sensitive: false }
  validates :homepage, presence: true
end
