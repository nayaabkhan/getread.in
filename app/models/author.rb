class Author < ActiveRecord::Base
  has_many :sources
  has_many :stories, through: :sources

  validates :name, presence: true
  validates :name, uniqueness: { case_sensitive: false }
  validates :bio, presence: true
  validates :slug, presence: true
  validates :slug, uniqueness: { case_sensitive: false }

  # how about a counter cache?
  def self.with_stories
    joins(:stories).uniq.all
  end

  def to_param
    slug
  end
end
