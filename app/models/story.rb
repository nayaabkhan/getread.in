class Story < ActiveRecord::Base
  belongs_to :source
  belongs_to :category

  validates :title, presence: true
  validates :url, presence: true
  validates :url, uniqueness: { case_sensitive: false }

  scope :uncategorized, proc { where(:category_id => nil) }

  after_save :enqueue_index_story
  after_destroy :enqueue_remove_story_index

  # always the most recent first
  default_scope { order('published_at DESC') }

  private

  def enqueue_index_story
    IndexStoryJob.perform_later self.id
  end

  def enqueue_remove_story_index
    RemoveStoryIndexJob.perform_later self.id
  end
end
