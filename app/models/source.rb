class Source < ActiveRecord::Base
  belongs_to :author
  belongs_to :site
  has_many :stories

  validates :author, presence: true
  validates :author, uniqueness: { scope: :site,
    message: 'already added for the site' }
  validates :site, presence: true
  validates :url, presence: true
  validates :url, uniqueness: { case_sensitive: false }

  def to_s
    "#{author.name} for #{site.name}"
  end
end
