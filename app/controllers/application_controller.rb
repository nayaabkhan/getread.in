class ApplicationController < ActionController::Base
  before_filter :populate_categories
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def populate_categories
    @navigations = Category.all
  end
end
