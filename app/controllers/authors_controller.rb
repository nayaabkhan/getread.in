class AuthorsController < ApplicationController
  def index
    @authors = Author.with_stories
  end

  def view
    @author = Author.find_by_slug(params[:slug])
  end
end
