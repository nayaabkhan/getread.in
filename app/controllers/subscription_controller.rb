class SubscriptionController < ApplicationController
  def all_weekly_digest
    email_address = params[:email_address]

    # simple email address validation
    if email_address !~ /@/
      success = false
      message = "The Email you gave isn't a valid one."
    else
      # check if exists already
      subscription = Subscription.find_by_email_address(email_address)
      if subscription
        success = true
        message = 'Looks like you are already signed up for our digest mails!'
      else
        if Subscription.create({:email_address => email_address})
          # add to weekly digest list
          SubscribeUserToWeeklyDigestJob.perform_later(email_address)

          success = true
          message = "Hello! We've signed you up for our weekly digest."

          # send greeting email
          DigestMailer.welcome(email_address).deliver_later
        else
          success = false
          message = "There was some problem subscribing you, try again in some time."
        end
      end

      cookies[:hide_subscription_banner] = {
        :value => true,
        :expires => 1.year.from_now
      };
    end

    respond_to do |format|
      format.json { render json:  {success: success, message: message} }
    end
  end
end
