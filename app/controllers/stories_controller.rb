class StoriesController < ApplicationController
  def index
    @story_groups = prepare_groups
  end

  def list
    @category = Category.find_by(slug: params[:category])
    @story_groups = prepare_groups(@category)
  end

  private
  def prepare_groups(category = nil)
    story_groups = Hash.new

    today = Story.where('published_at >= ?', Time.zone.now.beginning_of_day).order(published_at: :desc, created_at: :desc)
    today = today.where(category: category) if category
    story_groups[:today] = {
      :label => 'Today',
      :name => Date.today.strftime("%B") + ' ' + Date.today.day.ordinalize,
      :stories => today
    }

    yesterday = Story.where(:published_at => Date.yesterday.beginning_of_day..Date.yesterday.end_of_day).order(published_at: :desc, created_at: :desc)
    yesterday = yesterday.where(category: category) if category
    story_groups[:yesterday] = {
      :label => 'Yesterday',
      :name => Date.yesterday.strftime('%B') + ' ' + Date.yesterday.day.ordinalize,
      :stories => yesterday
    }

    week = Story.where(:published_at => 7.days.ago.beginning_of_day..2.days.ago.end_of_day).order(published_at: :desc, created_at: :desc)
    week = week.where(category: category) if category
    story_groups[:week] = {
      :label => 'This Week',
      :stories => week
    }

    month = Story.where(:published_at => 30.days.ago.beginning_of_day..8.days.ago.end_of_day).order(published_at: :desc, created_at: :desc)
    month = month.where(category: category) if category
    story_groups[:month] = {
      :label => 'This Month',
      :stories => month
    }

    older = Story.where('published_at < ?', 31.days.ago.end_of_day).order(published_at: :desc, created_at: :desc).limit(50)
    older = older.where(category: category) if category
    story_groups[:older] = {
      :label => 'Older',
      :stories => older
    }

    story_groups
  end
end
