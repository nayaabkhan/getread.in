class RemoveStoryIndexJob < ActiveJob::Base
  queue_as :default

  def perform(story_id)
    client = Swiftype::Client.new
    client.destroy_document(ENV['SWIFTYPE_ENGINE_SLUG'], 'stories', story_id)
  end
end
