class IndexStoryJob < ActiveJob::Base
  queue_as :default

  def perform(story_id)
    story = Story.find(story_id)
    category = story.category ? story.category.title : ''

    client = Swiftype::Client.new
    client.create_or_update_document(
      ENV['SWIFTYPE_ENGINE_SLUG'],
      'stories',
      {
        :external_id => story.id,
        :fields => [{ :name => 'title', :value => story.title, :type => 'string' },
                    { :name => 'synopsis', :value => story.synopsis, :type => 'text' },
                    { :name => 'url', :value => story.url, :type => 'enum' },
                    { :name => 'author', :value => story.source.author.name, :type => 'string' },
                    { :name => 'category', :value => category, :type => 'string' },
                    { :name => 'published_at', :value => story.published_at.iso8601, :type => 'date' }]
      }
    )
  end
end
