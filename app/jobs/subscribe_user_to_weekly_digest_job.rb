class SubscribeUserToWeeklyDigestJob < ActiveJob::Base
  queue_as :default

  def perform(email_address)
    gibbon = Gibbon::Request.new
    gibbon.lists(ENV["MAILCHIMP_WEEKLY_DIGEST_LIST_ID"]).members.create(
      body: {email_address: email_address, status: "subscribed"}
    )
  end
end
