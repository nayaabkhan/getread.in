ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel "Recent stories" do
          ul do
            Story.order(published_at: :desc).limit(10).map do |story|
              li link_to(story.title, admin_story_path(story))
            end
          end
        end
      end
    end
  end
end
