ActiveAdmin.register Story do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if resource.something?
#   permitted
# end
  permit_params :title, :url, :synopsis, :source_id, :published_at, :category_id

  scope :all, default: true
  scope :uncategorized

  index do
    selectable_column
    column :title
    column :url
    column :category
    column :created_at
    column :published_at
    actions
  end

end
