namespace :weekly_digest do
  desc "Send weekly digest mail to all subscribers"
  task send: :environment do
    log = ActiveSupport::Logger.new('log/digest.log')

    start_time = Time.now
    log.info "Task started at #{start_time}."

    # get stories from past week till now
    past_week_stories = Story.where('created_at > ?', 1.week.ago)
    log.info "Found #{past_week_stories.count} stories from past week."

    log.info "Fetching share counts for each..."
    # we use share count as a benchmark to decide popularity
    stories_with_share_count = Array.new
    past_week_stories.each do |story|
      share_count = ShareCount::for_facebook(story.url)
      stories_with_share_count.push({ :story => story, :share_count => share_count })
    end

    # get top 10 most shared stories first
    top_stories = stories_with_share_count.sort_by {|k| k[:share_count]}.reverse
    top_stories = top_stories[0..9]

    log.info "Found #{top_stories.count} top stories from past week."
    if top_stories.count
      log.info "Sending digest out to subscribers..."
      DigestMailer.send_weekly_digest(top_stories).deliver_now
    end

    end_time = Time.now
    duration = (end_time - start_time)
    log.info "Task finished at #{end_time} and lasted #{duration} seconds."
    log.close
  end
end
