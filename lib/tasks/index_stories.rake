namespace :search do
  desc "Index all existing stories"
  task index_stories: :environment do
    if ENV['SWIFTYPE_API_KEY'].blank?
      abort("SWIFTYPE_API_KEY not set")
    end

    if ENV['SWIFTYPE_ENGINE_SLUG'].blank?
      abort("SWIFTYPE_ENGINE_SLUG not set")
    end

    client = Swiftype::Client.new

    Story.find_in_batches(:batch_size => 10) do |stories|
      documents = stories.map do |story|
        category = story.category ? story.category.title : ''
        {
          :external_id => story.id,
          :fields => [{ :name => 'title', :value => story.title, :type => 'string' },
                      { :name => 'synopsis', :value => story.synopsis, :type => 'text' },
                      { :name => 'url', :value => story.url, :type => 'enum' },
                      { :name => 'author', :value => story.source.author.name, :type => 'string' },
                      { :name => 'category', :value => category, :type => 'string' },
                      { :name => 'published_at', :value => story.published_at.iso8601, :type => 'date' }]
        }
      end
      results = client.create_or_update_documents(ENV['SWIFTYPE_ENGINE_SLUG'], 'stories', documents)

      results.each_with_index do |result, index|
        puts "Could not create #{stories[index].title} (##{stories[index].id})" if result == false
      end
    end
  end
end
