namespace :scrap do
  desc "Start scraping all sources one by one"
  task start: :environment do
    log = ActiveSupport::Logger.new('log/scraper.log')

    start_time = Time.now
    log.info "Task started at #{start_time}."

    Source.find_each do |s|
      log.info "Loading source <id:#{s.id}> <url:#{s.url}>..."
      begin
        Scraper::fetch(s)
      rescue Exception => e
        log.error "Failed! Exception Reason: #{e.message}"
        next
      end
    end

    end_time = Time.now
    duration = (end_time - start_time)
    log.info "Task finished at #{end_time} and lasted #{duration} seconds."
    log.close
  end
end
