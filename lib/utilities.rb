module Utilities
  module_function
  def parse_date(date_string)
    begin
      parsed_date = Chronic.parse(date_string) || Date.parse(date_string)
    rescue
      parsed_date = Date.today
    end

    parsed_date.to_date
  end
end
