class CreateSubscriptions < ActiveRecord::Migration
  def change
    create_table :subscriptions do |t|
      t.string :email_address, index: true

      t.timestamps null: false
    end
  end
end
