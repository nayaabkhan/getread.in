class CreateStories < ActiveRecord::Migration
  def change
    create_table :stories do |t|
      t.string :title
      t.string :url
      t.string :synopsis
      t.references :source, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
