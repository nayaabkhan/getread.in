class AddHomepageToSites < ActiveRecord::Migration
  def change
    add_column :sites, :homepage, :string
  end
end
