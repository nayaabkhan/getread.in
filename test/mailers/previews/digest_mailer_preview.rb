# Preview all emails at http://localhost:3000/rails/mailers/digest_mailer
class DigestMailerPreview < ActionMailer::Preview
  def welcome
    DigestMailer.welcome('khannayaab@gmail.com')
  end

  def send_weekly_digest
    # we simple use 10 past week stories for previews
    past_week_stories = Story.where('created_at > ?', 1.week.ago).limit(10)

    DigestMailer.send_weekly_digest(past_week_stories)
  end
end
