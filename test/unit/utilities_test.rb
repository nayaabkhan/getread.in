require 'test_helper'

class UtilitiesTest < ActiveSupport::TestCase
  include Utilities

  test 'should parse mins ago date' do
    assert_equal 22.minutes.ago.to_date, parse_date('22 mins ago')
  end

  test 'should parse hours ago date' do
    assert_equal 10.hours.ago.to_date, parse_date('10 hours ago')
  end

  test 'should parse livemint kind of date' do
    d = Date.parse('2015-9-27')
    assert_equal d, parse_date('Sun, Sep 27 2015. 11 55 PM')
  end

  test 'should parse NDTV kind of date' do
    d = Date.parse('2015-9-27')
    assert_equal d, parse_date('Wednesday, September 27, 2015')
  end

  test 'should fallback to today if nothing works' do
    assert_equal Date.today.to_date, parse_date('Badada')
  end
end
